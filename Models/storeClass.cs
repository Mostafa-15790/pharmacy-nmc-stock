﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NMCStock.Models
{
    public class storeClass
    {
        public int store_id { get; set; }
        public string store_name { get; set; }
        public string store_address { get; set; }
    }
}