﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using Oracle.ManagedDataAccess.Client;
namespace NMCStock.Controllers
{
    public class getItemStockController : Controller
    {
               string SERVER = ConfigurationManager.ConnectionStrings["otcDB"].ConnectionString;

        // GET: getItemStock
        public ActionResult Index()
        {
            var StoreArrays = new List<Models.item_stock.storeClass>();

            string queryStringStore = "select store,store||' - '||store_name store_name from rmsowner.store";

            using (OracleConnection connection = new OracleConnection(SERVER))
            {
                OracleCommand command = new OracleCommand(queryStringStore)
                {
                    Connection = connection
                };
                try
                {
                    connection.Open();
                    OracleDataReader rdr = command.ExecuteReader();
                    if (rdr.HasRows == true)
                    {
                        //List<int> userFunctionCode = new List<int>();ins_co_policy_id
                        var i = 1;
                        while (rdr.Read())
                        {
                            Models.item_stock.storeClass ins_c = new Models.item_stock.storeClass();
                            ins_c.store_id = int.Parse(rdr["store"].ToString());
                            ins_c.store_name = rdr["store_name"].ToString();
                            StoreArrays.Add(ins_c);
                            i++;
                        }
                    }
                    rdr.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }




            }
            ViewBag.StoreArrays = StoreArrays;
            return View();
        }

        [HttpPost]
        public ActionResult getItems(string itemName, int itemID)
        {
            var itemArrays = new List<Models.item_stock.itemClass>();
            string queryString = "select item,short_desc,item_desc from item_master where LOWER(short_desc) like LOWER('%$itemName$%') and item_parent is null order by short_desc asc";
            queryString = queryString.Replace("$itemName$", itemName);
            using (OracleConnection connection = new OracleConnection(SERVER))
            {
                OracleCommand command = new OracleCommand(queryString)
                {
                    Connection = connection
                };
                try
                {
                    //command.Parameters.Add(":itemName", itemName);
                    connection.Open();
                    OracleDataReader rdr = command.ExecuteReader();
                    if (rdr.HasRows == true)
                    {
                        //List<int> userFunctionCode = new List<int>();ins_co_policy_id
                        var i = 1;
                        while (rdr.Read())
                        {
                            Models.item_stock.itemClass ins_c = new Models.item_stock.itemClass();
                            ins_c.item_id = int.Parse(rdr["item"].ToString());
                            ins_c.item_name = rdr["short_desc"].ToString();
                            itemArrays.Add(ins_c);
                            i++;
                        }
                    }
                    rdr.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            return Json(new { success = true, Item_list = itemArrays, errorr = "" });
        }


        public ActionResult getResultStock(string[] itemNameArray, int storeId)
        {
            string[] arr = (string[])itemNameArray;
            var itemArrays = new List<Models.item_stock.itemClass>();
            for (int i = 0; i < arr.Length; i++)
            {
                string queryString = "select m.id_itm, m.total_quantity -  (m.reserved_quantity + m.customer_resv_quantity + m.rtv_quantity + m.adjust_unavail_qty) total_quantity from simowner.rk_store_item_soh m where m.id_str_rt = $store_id$ and  m.id_itm = $arr$";
                queryString = queryString.Replace("$store_id$", storeId + "");
                queryString = queryString.Replace("$arr$", arr[i]);
                using (OracleConnection connection = new OracleConnection(SERVER))
                {
                    OracleCommand command = new OracleCommand(queryString)
                    {
                        Connection = connection
                    };
                    try
                    {
                        connection.Open();
                        OracleDataReader rdr = command.ExecuteReader();
                        if (rdr.HasRows == true)
                        {
                            while (rdr.Read())
                            {
                                Models.item_stock.itemClass ins_c = new Models.item_stock.itemClass();
                                if (rdr["total_quantity"].ToString() != "")
                                    ins_c.item_quantity = int.Parse(rdr["total_quantity"].ToString());
                                else
                                    ins_c.item_quantity = 0;
                                itemArrays.Add(ins_c);
                            }
                        }
                        else
                        {
                            Models.item_stock.itemClass ins_c = new Models.item_stock.itemClass();
                            ins_c.item_quantity = 0;
                            itemArrays.Add(ins_c);
                        }
                        rdr.Close();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
            return Json(new { success = true, Item_list = itemArrays, errorr = "" });
        }
    }



}